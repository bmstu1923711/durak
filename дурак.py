
def check_gamer_turn(table, gamer_card):
    count=0
    if table==[]:
        return
    for i in table:
        if i[1]==gamer_card[1]:
            count+=1
            return
    if count==0:
        print("Неверная карта!")
        print("Напишите номер карты")
        print("Ваша колода")
        print_cards(gamer_hand)
        card=int(input())
        check_gamer_turn(table, gamer_card)
def check_computer_turn(computer_card, gamer_card):
    count=0
    print(computer_card, gamer_card)
    if computer_card[1]==gamer_card[1]:
        count+=1
        return
    if count==0:
        print("Неверная карта!")
        print("Напишите номер карты")
        print("Ваша колода")
        print_cards(gamer_hand)
        gamer_card=gamer_hand[int(input())-1]
        check_computer_turn(table, gamer_card)
def check_gamer_hand(gamer_hand, table):
    count=0
    if table==[]:
        return
    for i in table:
        for k in gamer_hand:
            if i[1]==k[1]:
                print("Противник взял карту. Будете ходить?")
                print("Ваша колода")
                print_cards(gamer_hand)
                count+=1
                return
    if count==0:
        return

import random
import copy
koloda = []
suits = ['♠', '♥', '♣', '♦']
rank = [400, 300, 200, 100, 0, -100, -200, -300, -400]
trump_card = (suits[random.randint(0, 3)], rank[random.randint(0, 8)])
gamer_hand = []
computer_hand = [ ]
table=[]
rank_name = {400: 'Туз', 300: 'Король', 200: 'Дама', 100: 'Валет', 0: '10', -100: '9', -200: '8', -300: '7', -400: '6'}
def print_cards(obj):
    if type(obj) == tuple:
        if obj[1]>=500:
            name = rank_name[obj[1]-900]
        else:
            name = rank_name[obj[1]]
        print(obj[0], name)
    else:
        num = 0
        for i in obj:
            num +=1
            if i[1]>=500:
                name = rank_name[i[1]-900]
            else:
                name = rank_name[i[1]]
            print(str(num)+'.', i[0], name)

def coloda(rank, suits, trump_card):
    for i in rank:
        for j in suits:
            if j != trump_card[0]:
                card = (j, i,)
            else:
                card = (j, i+900,)
            koloda.append(card)
def distribution_gamer(koloda):
    while len(gamer_hand)<6 and koloda!=[]:
        card=koloda[random.randint(0, len(koloda)-1)]
        gamer_hand.append(card)
        koloda.remove(card)
def  distribution_computer(koloda):
    while len(computer_hand)<6 and koloda!=[]:
        card=koloda[random.randint(0, len(koloda)-1)]
        computer_hand.append(card)
        koloda.remove(card)
def gamer_turn(gamer_hand, computer_hand, trump_card, table):
    if gamer_hand==[] and koloda==[]:
        return
    print("Ваша колода")
    print_cards(gamer_hand)
    print("Ваш ход! Напишите номер карты")
    gamer_card=gamer_hand[int(input())-1]
    check_gamer_turn(table, gamer_card) #new
    table.append(gamer_card)
    gamer_hand.remove(gamer_card)
    if gamer_hand==[] and koloda==[]:
        return
    a=1500 #значение достоинства мин карты

    for i in computer_hand:
        if i[0]==gamer_card[0] and i[1]>gamer_card[1] and i[1]<a:
            a=i[1]
            suit = gamer_card[0]
        elif i[1]>400 and i[1]<a and gamer_card[1]<500:
            a=i[1]
            suit=trump_card[0]
    if a==1500:

        print("Противник взял карту. Будете ходить?")
        print("Ваша колода")
        print_cards(gamer_hand)
        ans=str(input()).lower()
        if ans=="да":
            while ans=="да":
                print("Напишите номер карты")
                print("Ваша колода")
                print_cards(gamer_hand)
                card=int(input())
                check_gamer_turn(table, gamer_card) #new
                table.append(gamer_hand[card-1])
                gamer_hand.remove(gamer_hand[card-1])
                if gamer_hand==[] and koloda==[]:
                    return
                print("Еще ход?")
                ans=str(input()).lower()
        while len(table)!=0:
            for i in table:
                computer_hand.append(i)
                table.remove(i)
    else:
        computer_card=(suit, a)
        computer_hand.remove(computer_card)
        if computer_hand==[] and koloda==[]:
            return
        table.append(computer_card)
        print("Карта противника")
        print_cards(computer_card)
        print("Будете ходить?")
        ans=str(input()).lower()
        if ans=="да":
            gamer_turn(gamer_hand, computer_hand, trump_card, table)
        else:
            table.clear()
    table.clear()

def computer_turn(gamer_hand, computer_hand, trump_card, table):
    end=0
    a=1500
    probable_card=[]
    for i in computer_hand:
        if i[1]<a:
            a=i[1]
            suit = i[0]
    computer_card=(suit, a)
    probable_card.append(a)
    if a<500:
        probable_card.append(a+900)
    computer_hand.remove(computer_card)
    if computer_hand==[] and koloda==[]:
        return
    table.append(computer_card)
    print("Карта противника")
    print_cards(computer_card)
    print("Ваша колода")
    print_cards(gamer_hand)
    print("Будете ходить?")
    ans=str(input()).lower()
    if ans=="да":
        count=1
        while ans=="да":
            print("Напишите номер карты")
            while count!=0:
                gamer_card=gamer_hand[int(input())-1]
                check_computer_turn(table, gamer_card) #new
                gamer_hand.remove(gamer_card)
                if gamer_hand==[] and koloda==[]:
                    return
                print("Ваша колода")
                print_cards(gamer_hand)
                probable_card.append(gamer_card[1])
                count-=1
            if gamer_card[1]<500:
                probable_card.append(gamer_card[1]+900)
            table.append(gamer_card)
            copy_computer_hand=copy.deepcopy(computer_hand)
            for i in copy_computer_hand:
                if i[1] in probable_card:
                    suit = i[0]
                    computer_card=(suit, i[1])
                    computer_hand.remove(computer_card)
                    if computer_hand==[] and koloda==[]:
                        return
                    print("Карта противника")
                    print_cards(computer_card)
                    table.append(computer_card)
                    count+=1
            if count>0:
                print("Ваша колода")
                print_cards(gamer_hand)
                print("Будете ходить?")
                ans=str(input()).lower()
            else:
                table.clear()
                probable_card.clear()
                break
    if ans=="нет":
        copy_computer_hand=copy.deepcopy(computer_hand)
        for i in copy_computer_hand:
                if i[1] in probable_card:
                    suit = i[0]
                    computer_card=(suit, i[1])
                    computer_hand.remove(computer_card)
                    if computer_hand==[] and koloda==[]:
                        break
                    table.append(computer_card)
        copy_table=copy.deepcopy(table)
        while len(table)!=0:
            for i in copy_table:
                gamer_hand.append(i)
                table.remove(i)
    table.clear()
    probable_card.clear()

def game():
    print("Козырь", trump_card[0])
    coloda(rank, suits, trump_card)
    distribution_gamer(koloda)
    distribution_computer(koloda)
    def game1():
        while gamer_hand!=[] or computer_hand!=[]:
            p=0
            distribution_gamer(koloda)
            distribution_computer(koloda)
            print("Длина общей колоды", len(koloda))
            print("Длина колоды противника", len(computer_hand))
            x=len(gamer_hand)
            y=len(computer_hand)
            gamer_turn(gamer_hand, computer_hand, trump_card, table)
            if gamer_hand==[]:
                print("Вы выиграли!")
                break
            elif computer_hand==[]:
                print("Вы проиграли")
                break
            while len(computer_hand)>y:
                distribution_computer(koloda)
                distribution_gamer(koloda)
                print("Длина общей колоды", len(koloda))
                x=len(gamer_hand)
                y=len(computer_hand)
                gamer_turn(gamer_hand, computer_hand, trump_card, table)
                if gamer_hand==[]:
                    p+=1
                    print("Вы выиграли!")
                    break
                elif computer_hand==[]:
                    p+=1
                    print("Вы проиграли")
                    break
            if p!=0:
                break
            distribution_computer(koloda)
            distribution_gamer(koloda)
            print("Длина общей колоды", len(koloda))
            print("Длина колоды противника", len(computer_hand))
            x=len(gamer_hand)
            y=len(computer_hand)
            computer_turn(gamer_hand, computer_hand, trump_card, table)
            if gamer_hand==[]:
                print("Вы выиграли!")
                break
            elif computer_hand==[]:
                print("Вы проиграли")
                break
            while len(gamer_hand)>x:
                distribution_gamer(koloda)
                distribution_computer(koloda)
                print("Длина общей колоды", len(koloda))
                print("Длина колоды противника", len(computer_hand))
                x=len(gamer_hand)
                y=len(computer_hand)
                computer_turn(gamer_hand, computer_hand, trump_card, table)
                if gamer_hand==[]:
                    p+=1
                    print("Вы выиграли!")
                    break
                elif computer_hand==[]:
                    p+=1
                    print("Вы проиграли")
                    break
            if p!=0:
                break
    game1()

import random, copy
koloda = []
suits = ['♠', '♥', '♣', '♦']
rank = [400, 300, 200, 100, 0, -100, -200, -300, -400]
trump_card = (suits[random.randint(0, 3)], rank[random.randint(0, 8)])
gamer_hand = []
computer_hand = [ ]
table=[] #new
end=0 #условие конца
game()
